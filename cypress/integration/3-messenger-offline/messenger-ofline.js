describe("User flow", () => {
  it("describes typical user flow, offline", () => {
    cy.visit("http://localhost:3000");

    cy.get("form").find("#input").should("exist").type("Hi");
    cy.get("form").find("[type=submit]").should("not.be.disabled");
    cy.get("form").find("[type=submit]").should("not.be.disabled");
    cy.window().trigger("offline");

    cy.get("form").find("[type=submit]").should("be.disabled");
    cy.get("form").find("[type=submit]").should("be.disabled");
    cy.findByRole("alert").should("exist");

    cy.wait(2000);

    cy.window().trigger("online");
    cy.get("form").find("[type=submit]").should("not.be.disabled");
    cy.get("form").find("#input").should("not.be.disabled");
    cy.findByRole("alert").should("not.exist");
  });
});

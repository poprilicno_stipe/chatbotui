describe("User flow", () => {
    it("describes typical user flow, text bot response", () => {
        cy.visit("http://localhost:3000");

        cy.get("[data-testid='box']").findByRole("progressbar").should("exist");
        cy.get("[data-testid='box']").findByRole("progressbar").should("not.exist");

        const userInput ="Can't wait till I am doing ... well... not this.";
        const userInput2 = "Can't find that good cheese burek like we have in Zagreb. Here they only sell that circular Greek or Turkish or Bosniak thing. Not enough cheese. ";
        cy.get("form").find("Button").should("exist").should("be.disabled");
        cy.get("form").find("#input").should("exist").type(userInput);
        cy.get("form").find("[type=submit]").should("not.be.disabled").click();
        cy.get("form").find("[type=submit]").should("be.disabled");
        cy.get("form").find("#input").should("have.value", "");

        cy.findByText(userInput).should("exist");
        cy.findByText("Hello there!").should("exist");
        cy.findByText(`You said: ${userInput}`).should("exist");

        cy.findAllByTestId("user-message").should("have.length", 1);
        cy.findAllByTestId("bot-message").should("have.length", 2); 
    });
});

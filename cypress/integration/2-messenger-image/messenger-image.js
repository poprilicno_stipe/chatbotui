describe("User flow", () => {
  it("describes typical user flow, image bot response", () => {
    cy.visit("http://localhost:3000");

    const userInput3 = "cat image";
    cy.get("form").find("#input").should("exist").type(userInput3);
    cy.get("form").find("[type=submit]").click();

    cy.findByText(userInput3).should("exist");
    cy.findByText(`Look a cat!`).should("exist");

    cy.wait(1000);

    cy.findAllByTestId("user-message").should("have.length", 1);
    cy.findAllByTestId("bot-message").should("have.length", 2);
    cy.get("article").find("img").should("have.attr", "src");
  });
});

import React from "react";
import { useDispatch } from "react-redux";
import { v4 as universallyUniqueIdentifierV4 } from "uuid";
import { appSocketClient } from "../middleware/socketClient";
import { sendMessage, Messenger } from "../features/Messenger";
import { Box, CircularProgress } from "@mui/material";

function App() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState<string | null>(null);
  const dispatch = useDispatch();

  React.useEffect(() => {
    appSocketClient.connect().then(
      () => setIsLoading(false),
      (e) => setError(e.toString)
    );

    appSocketClient.on("output", (output) => {
      dispatch(
        sendMessage({
          id: universallyUniqueIdentifierV4(),
          text: output.text,
          author: "bot",
          data: (output.data) ? output.data : null, 
        })
      );
    });

    appSocketClient.on("error", ({ message }) => {
      setError(message);
      console.log(message);
    });

    return () => {
      appSocketClient.disconnect();
    };
  }, []);
  return isLoading ? (
    <Box
      data-testid="box"
      display="flex"
      justifyContent="center"
      alignItems="center"
      minHeight="100vh"
    >
      <CircularProgress style={{ color: "#000" }} />
    </Box>
  ) : (
    <Messenger />
  );
}

export default App;

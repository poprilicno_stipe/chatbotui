import {combineReducers} from "redux";
import MessagesReducer from "../features/Messenger/reducer";

export default combineReducers({
    messages: MessagesReducer,
});

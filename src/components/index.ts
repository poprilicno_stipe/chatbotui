export { default as HumanUserSVG } from "./humanUserSVG";
export { default as SendArrowSVG } from "./sendArrowSVG";
export { default as ArtificialIntelligenceSVG } from "./artificialIntelligenceSVG";

export interface Message {
  id: string;
  text: string;
  author: "user" | "bot";
  data?: any;
}

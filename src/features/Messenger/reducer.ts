import { ActionTypes, SEND_MESSAGE } from "./actions";
import { Message } from "./types";

const initialState: Message[] = [];

const MessagesReducer = (
  state: Message[] = initialState,
  action: ActionTypes
): Message[] => {
  switch (action.type) {
    case SEND_MESSAGE:
      return [...state, action.payload];
    default:
      return state;
  }
};

export default MessagesReducer;

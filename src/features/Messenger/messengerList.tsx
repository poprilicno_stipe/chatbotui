import React, { ReactElement } from "react";
import { useSelector } from "react-redux";
import { Message } from "./types";
import { getMessages } from "./selectors";
import MessageEntry from "./messageEntry";
import { useStyles } from "./styles";

function MessengerList(): ReactElement {
  const classes = useStyles();
  const ref = React.useRef<HTMLDivElement | null>(null);
  const messages = useSelector(getMessages);

  React.useEffect(() => {
    if (ref?.current) {
      ref.current.scrollTop = ref.current.scrollHeight;
    }
  });

  return (
    <section className={classes.messengerItemsList} ref={ref}>
      {messages.map((message: Message) => (
        <MessageEntry message={message} key={message.id} />
      ))}
    </section>
  );
}

export default MessengerList;

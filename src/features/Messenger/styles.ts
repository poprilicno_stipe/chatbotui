import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "0.4em",
      background: "transparent",
      border: "dotted 1px #41403E",
    },
    "*::-webkit-scrollbar-track": {
      "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
    },
    "*::-webkit-scrollbar-thumb": {
      outline: "2px solid #41403E",
    },
  },
  messengerContainer: {
    display: "flex",
    height: "95vh",
    flexDirection: "column",
  },
  messengerItemsList: {
    flexGrow: 1,
    overflowY: "auto",
  },
  bubble: {
    display: "flex",
    flexDirection: "row",
    width: "fill-available",
    minHeight: "2rem",
    padding: "0.25rem",
    alignItems: "center",
    wordBreak: "break-all",
    overflowWrap: "break-word",
    fontFamily: "Amatic SC",
    fontSize: "1.75rem",
    fontWeight: "bold",
    letterSpacing: "0.25rem",
    '& p':{
      margin:"0",
    },
    '& img':{
      maxWidth:"10rem",
    }
  },
  handDrawnBox: {
    background: "transparent",
    padding: "0.5rem 0.5rem",
    margin: "1rem 0",
    borderTopLeftRadius: "255px 15px",
    borderTopRightRadius: "15px 225px",
    borderBottomRightRadius: "225px 15px",
    borderBottomLeftRadius: "15px 255px",
    border: "solid 2px #41403E",
  },
  right: {
    flexFlow: "row-reverse",
    border: "dashed 2px #4d4b48",
  },
  left: {
    justifyContent: "flex-start",
    alignSelf: "start",
    border: "solid 2px #41403E",
  },
  textField: {
    flexGrow: 1,
    margin: "0",
    border: "solid 3px #41403E",
    fontFamily: "Amatic SC",
    fontSize: "2.2rem",
    fontWeight: "bold",
    letterSpacing: "0.3rem",
  },
}));

import { Dispatch } from "redux";
import { Message } from "./types";

export const SEND_MESSAGE = "SEND_MESSAGE";

interface MessageAction {
  type: typeof SEND_MESSAGE;
  payload: Message;
}

export const sendMessage =
  (message: Message) =>
  (dispatch: Dispatch): void => {
    dispatch({
      type: SEND_MESSAGE,
      payload: message,
    });
  };

export type ActionTypes = MessageAction;

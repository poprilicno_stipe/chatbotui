import { RootState } from "../../app/store";
import { Message } from "./types";

export const getMessages = (state: RootState): Message[] => state.messages;

import React, { ReactElement } from "react";
import { useDispatch } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import { sendMessage } from "./actions";
import { appSocketClient } from "../../middleware/socketClient";
import { Alert, Box, Button, InputBase } from "@mui/material";
import SendArrowSVG from "../../components/sendArrowSVG";
import { useStyles } from "./styles";

function MessengerInput(): ReactElement {
  const classes = useStyles();
  const [text, setText] = React.useState("");
  const [isOnline, setIsOnline] = React.useState(true);
  const dispatch = useDispatch();

  const handleOffline = () => setIsOnline(false);
  const handleOnline = () => setIsOnline(true);

  React.useEffect(() => {
    window.addEventListener("online", handleOnline);
    window.addEventListener("offline", handleOffline);

    return () => {
      window.removeEventListener("online", handleOnline);
      window.removeEventListener("offline", handleOffline);
    };
  }, []);

  const onFormSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    appSocketClient.sendMessage(text);
    dispatch(
      sendMessage({
        id: uuidv4(),
        text: text,
        author: "user",
      })
    );
    setText("");
  };

  return (
    <>
      {!isOnline ? (
        <Alert severity="error">No internet connection!</Alert>
      ) : null}
      <form onSubmit={onFormSubmit}>
        <Box sx={{ display: "flex", alignItems: "flex-end" }}>
          <InputBase
            id="input"
            value={text}
            disabled={!isOnline}
            onChange={(event) => setText(event.target.value)}
            className={classes.textField + " " + classes.handDrawnBox}
          />
          <Button
            variant="text"
            endIcon={<SendArrowSVG />}
            type="submit"
            disabled={!isOnline || !text.length}
            aria-label="send"
          />
        </Box>
      </form>
    </>
  );
}

export default MessengerInput;

import { ReactElement } from "react";
import { Message } from "./types";
import { Avatar } from "@mui/material";
import { ArtificialIntelligenceSVG, HumanUserSVG } from "../../components";
import { useStyles } from "./styles";

function MessageEntry({ message }: { message: Message }): ReactElement {
  const classes = useStyles();
  const isBot = message.author === "bot";
  const alignClass = isBot ? "left" : "right";

  return (
    <article
      className={
        classes.bubble + " " + classes[alignClass] + " " + classes.handDrawnBox
      }
    >
      <Avatar
        style={{
          backgroundColor: "#fff",
          margin: "0 1rem",
        }}
      >
        {message.author === "user" ? (
          <HumanUserSVG />
        ) : (
          <ArtificialIntelligenceSVG />
        )}
      </Avatar>
      <div
        data-testid={isBot ? "bot-message" : "user-message"}
      >
        <div>
          <p>{message.text}</p>
          {message.data?.imgSrc ? <img src={message.data.imgSrc} /> : null}
        </div>
      </div>
    </article>
  );
}

export default MessageEntry;

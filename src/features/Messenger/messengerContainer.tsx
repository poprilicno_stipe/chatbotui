import { ReactElement } from "react";
import MessengerInput from "./messengerInput";
import MessengerList from "./messengerList";
import { Container } from "@mui/material";
import { useStyles } from "./styles";

function MessengerContainer(): ReactElement {
  const classes = useStyles();
  return (
    <Container className={classes.messengerContainer} maxWidth="sm">
      <MessengerList />
      <MessengerInput />
    </Container>
  );
}

export default MessengerContainer;

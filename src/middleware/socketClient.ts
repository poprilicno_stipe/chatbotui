import { SocketClient } from "@cognigy/socket-client";

const appSocketClient = new SocketClient(
  process.env.APP_SOCKET_URL as string,
  process.env.APP_SOCKET_TOKEN as string,
  {
    forceWebsockets: true,
    reconnectionLimit: 0,
  }
);

export { appSocketClient };

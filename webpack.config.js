const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const dotenv = require("dotenv");

const prod = process.env.NODE_ENV === "production";
const result = dotenv.config({ path: prod ? ".env" : ".env.development" });

module.exports = {
  mode: process.env.NODE_ENV || "development",
  entry: path.join(__dirname, "src", "index.tsx"),
  output: { path: path.join(__dirname, "build"), filename: "index.bundle.js" },
  resolve: { extensions: [".tsx", ".ts", ".js"] },
  devServer: { static: path.join(__dirname, "src") },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ["ts-loader"],
      },
      {
        test: /\.(css|scss)$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
        use: ["file-loader"],
      },
    ],
  },
  devtool: prod ? undefined : "source-map",
  plugins: [
    new HtmlWebpackPlugin({
      meta: {
        charset: {
          key: "charset",
          content: "utf-8",
        },
        viewport: {
          key: "viewport",
          content: "width=device-width, initial-scale=1",
        },
        "theme-color": {
          key: "theme-color",
          content: "#000000",
        },
        description: {
          key: "description",
          content:
            "Web-based Chatbot UI that connects to Cognigy and can be used to have a conversation with a Cognigy Bot",
        },
      },
      title: "Chatbot UI",
      template: path.join(__dirname, "src", "index.html"),
    }),
    new webpack.DefinePlugin({
      process: { env: JSON.stringify(result.parsed) },
    }),
  ],
};

# README

Web-based Chatbot UI that connects to Cognigy and can be used to have a conversation with a Cognigy Bot

### What is this repository for?

- Well... it's a bit obvious. Main point is that I made some interesting design choices. :)
- 1.2.0
- [Live demo on AWS S3](https://chatbotuiproject.s3.eu-central-1.amazonaws.com/index.html), not sure how long will it be available

### How do I get set up?

- First, clone this project with: git clone https://poprilicno_stipe@bitbucket.org/poprilicno_stipe/chatbotui.git
- Go to the cloned project folder on your machine, and install the project with: npm run i
- To run this project enter this into Your terminal: npm run start
- To run some basic test run: npm run test
- To build for production purposes please feel free to run: npm run build

### Requirements

- Your application source code is written in TypeScript
- Your application connects to a predefined existing Cognigy.AI bot
- Your application features a "text input" field with a "send" button
- Both "clicking the send button" and "hitting return" should submit your message
- Messages should be sent/received through our SocketClient
- Incoming and outgoing messages should be visually rendered in a chat history with "message bubbles"
- The incoming and outgoing "message bubbles" should be distinguishable by alignment and color

### Required tooling:

- react, react-dom
- redux, react-redux
- @material-ui/core
- @cognigy/socketclient

### Things to look out for:

- The application should have a README file that describes the application and contains all necessary steps to launch it
- The source code should make proper use of TypeScript
- CSS styling should be done utilizing material-ui's makeStyles, styled or withStyles
  mechanisms
- Make use of the npm packages mentioned in the instructions
- Make sure your files are properly structured and your code follows a consistent style
- Put attention to the overall visual style of the application
- Put attention to the usability of the application

### Bonus Tasks

- Messages from the bot can contain more than just text! If you send the text cat image to the bot, it will
  reply with a message that does not just contain text, but also a data payload with an image URL. Make
  your chat compatible so it will render message bubbles that contain both, the text as well as the image (if
  there is any)
- Write automated tests for your application. It can be a test suite of your choice! The important thing to look
  out for is that you are testing "behavior". The tests should be executable via a test script in your
  package.json
- Instead of using "zero-config" tools like create-react-app, parcel or the "zero config" modes of
  webpack and rollup, use a custom build configuration with a bundler you like.
